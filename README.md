# Don't Crash

A tank robot made with J5 & Nuxt

## Calibration

Calibration system to turn motors on/off

## To do

- Hook and control HCSR-04 to detect objects in auto mode so robot stops

## Build Setup

```bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm start
```
